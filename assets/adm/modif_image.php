<?php include("menu.php"); ?>
<?php
try
{ // On se connecte à MySQL
  $bdd = new PDO('mysql:host=localhost;dbname=Mini-pinterest;charset=utf8', 'root', '');
}
catch(Exception $e)
{ // En cas d'erreur, on affiche un message et on arrête tout
  die('Erreur : '.$e->getMessage());
}
// Si tout va bien, on peut continuer


$stmt = $bdd->prepare('SELECT * FROM Photo p WHERE nomFich= ?');
$stmt->bindParam(1, $_GET['n']);
$stmt->execute();

$donne = $stmt->fetch(PDO::FETCH_ASSOC);

?>

<div class="container">
  <div class="justify-content-center">
    <p class="h4"> Saisissez le(s) champ(s) que vous souhaiter modifier et cliquer sur "envoyer" </p></br>
    <p class="h5"> <?php echo $donne['nomFich'];?></p></br>
    <form method="POST" class="was-validated">
      <div class="form-col">
        <div class="form-group row-md-2 col-md-6">
          <label>Description</label>
          <textarea type="text" class="form-control is-invalid" name="description" required><?php echo $donne['description']; ?></textarea>
        </div>
        <div class="invalid-feedback">
          Please write at least one word.
        </div>
        <div class="col-md-3 mb-3">
          <label for="validationCustom04">Categorie</label>
          <select name="pCat" class="custom-select" id="validationCustom04" required>
            <option selected disabled value="" >Choose...</option>
            <option value="1">Animaux</option>
            <option value="2">Autres</option>
            <option value="3">Héroïnes</option>
          </select>
        </div>
<p><i>N'oubliez pas de sélectionner la catégorie avant d'envoyer vos modifications!</i></p>
       </div>
    </br>
    </br>
    <center><button type="submit" class="btn btn-success" name="update">Envoyer</button></center>
    </form>
  </div>
</div>

<?php

if(isset($_POST['update'])){
  $NewCategorie = $_POST['pCat'];
  echo $NewCategorie;
  $NewDescription = $_POST['description'];
  echo $NewDescription;
  $NomFich = $_GET['n'];
  echo $NomFich;

  $req = $bdd->prepare('UPDATE Photo SET description=?, catId=?  where nomFich=? ');
  $req->bindParam(1, $NewDescription);
  $req->bindParam(2, $NewCategorie);
  $req->bindValue(3, $NomFich);

  $req->execute();
  header("Location: http://localhost/mini-pinterest/assets/adm/image.php?n=$NomFich");
}

?>
