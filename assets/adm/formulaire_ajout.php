<?php session_start(); ?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Mille et une Images</title>

  <link rel="stylesheet" href="../../style.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <script src="script.js"></script>

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Mini-Pinterest</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="accueil.php">Accueil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link">  <?php echo $_SESSION['nom']; echo " "; echo $_SESSION['prenom']; ?> </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../../index.php">Déconnexion</a>
        </li>
      </ul>
    </div>
  </nav>
</head>

<body>

  <input type="text" class="form-control " id="validationServer03" required>
     <div class="invalid-feedback">
       Please provide a valid city.
     </div>

  <h4>Ajouter les informations relatives à l'image</h4></br>

<?php echo 'Votre image a bien été ajoutée ! Merci de remplir les informations correspondantes.' ;?></br>
  <form action="ajout_bdd.php" method="post" class="was-validated">
    <div class="form-group align-items-center">
      <div class="form-group">
        <label for="exampleFormControlTextarea1">Description</label>
        <textarea class="form-control is-invalid" id="exampleFormControlTextarea1" rows="3" name="pDesc" value="description" required></textarea>
      </div>
      <div class="invalid-feedback">
        Please write at least one word.
      </div>
      <div class="col-md-3 mb-3">
        <label for="validationCustom04">Categorie</label>
        <select name="pCat" class="custom-select" id="validationCustom04" required>
          <option selected disabled value="" >Choose...</option>
          <option value="1">Animaux</option>
          <option value="2">Autres</option>
          <option value="3">Héroïnes</option>
        </select>
      </div>

      <button type="submit" class="btn btn-success" name="pEnvoyer" value="Valider">Envoyer</button>

    </div>
  </form>

</body>
</html>
