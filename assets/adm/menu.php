<?php session_start();

$hourrefresh = strtotime(date('h:i:s'));
$TpsCo = date("i:s", $hourrefresh - $_SESSION['time']);

?>
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="style.css"/>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <title>Mille et une Images</title>
<center><h1>Mille et une Images</h1></center>
</head>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Mini-Pinterest</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="accueil.php">Accueil</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categorie</a>
        <!-- bloc menu déroulant -->
      <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <a class="dropdown-item" href="categorie.php?n=1"> Animaux </a>
        <a class="dropdown-item" href="categorie.php?n=2"> Autre </a>
        <a class="dropdown-item" href="categorie.php?n=3"> Héroïnes </a>
      </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="ajouterimage.php"> Ajouter une image</a>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['prenom'];echo " "; echo $_SESSION['nom'];?></a>
        <!-- bloc menu déroulant -->
      <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <a class="dropdown-item" href="compte.php">Mon Compte</a>
        <a class="dropdown-item" href="Statistique.php">Statistiques</a>
      </div>
      </li>
      <li class="nav-item">
        <a class="nav-link"> <?php echo "Connecté depuis "; echo $TpsCo ;?></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../../index.php">Déconnexion</a>
      </li>

  </ul>
  </div>
</nav>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
