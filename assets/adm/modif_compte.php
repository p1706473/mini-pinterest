<?php
include("menu.php");
try
{ // On se connecte à MySQL
  $bdd = new PDO('mysql:host=localhost;dbname=Mini-pinterest;charset=utf8', 'root', '');
}
catch(Exception $e)
{ // En cas d'erreur, on affiche un message et on arrête tout
  die('Erreur : '.$e->getMessage());
}
// Si tout va bien, on peut continuer

// On récupère tout le contenu de la table mini-pinterest
$stmt = $bdd->prepare('SELECT * FROM User WHERE Id_user= ?');
$stmt->bindParam(1, $_SESSION['Id_user']);
$stmt->execute();
$donne = $stmt->fetch(PDO::FETCH_ASSOC);


if(isset($_POST['update'])){
  $NewNom = $_POST['Nom'];
  $NewPrenom = $_POST['Prenom'];
  $NewIdentifiant = $_POST['Identifiant'];
  $NewMail = $_POST['Mail'];
  $NewMdp = $_POST['Mdp'];
  $Newmdpc = $_POST['MdpC'];
  $Id_user = $_SESSION['Id_user'];

  $req = $bdd->prepare('UPDATE User SET Nom=?, Prenom=?, Identifiant=?, Mail=?, Mdp=? where Id_user=? ');
  $req->bindParam(1, $NewNom);
  $req->bindParam(2, $NewPrenom);
  $req->bindParam(3, $NewIdentifiant);
  $req->bindValue(4, $NewMail);
  if($NewMdp = $Newmdpc){
    $req->bindValue(5, $NewMdp);
  }
  $req->bindValue(6, $Id_user);

  $req->execute();
  header('Location: http://localhost/mini-pinterest/assets/adm/compte.php');
}
?>


<div class="container">
  <p class="h4"> Saisissez le(s) champ(s) que vous souhaiter modifier et cliquer sur "envoyer" </p></br>
  <p class="h5">Identifiant utilisateur : <?php  echo $_SESSION['Id_user'];?></p></br>
  <form method="POST">
    <div class="form-col">
      <div class="col align-self-center">
      <div class="form-group col-md-3">
        <label>Nom</label>
        <input type="text" class="form-control" name="Nom" value="<?php echo $donne['Nom']; ?>">
      </div>
      <div class="form-group col-md-3">
        <label>Prenom</label>
        <input type="text" class="form-control" name="Prenom" value="<?php echo $donne['Prenom'];?>">
      </div>
      <div class="form-group col-md-3">
          <label>Identifiant </label>
          <input type="text" class="form-control" name="Identifiant" value="<?php echo $donne['Identifiant']; ?>">
        </div>
        <div class="form-group col-md-2">
          <label for="inputPassword4">Password</label>
          <input type="password" class="form-control" name="Mdp" value="<?php echo $donne['Mdp']; ?>">
        </div>
        <div class="form-group col-md-2">
          <label for="inputPassword5">Confirm Password</label>
          <input type="password" class="form-control" name="MdpC" value="<?php echo $donne['Mdp'];?>">
        </div>
        <div class="form-group col-md-3">
          <label for="inputEmail4">Email</label>
          <input type="email" class="form-control" name="Mail" value="<?php echo $donne['Mail'];?>">
        </div>
    </div>
  </div>
</br>
    <button type="submit" class="btn btn-success" name="update">Envoyer</button>
  </form>
</div>
