<?php
include("menu.php");
try
{ // On se connecte à MySQL
  $bdd = new PDO('mysql:host=localhost;dbname=Mini-pinterest;charset=utf8', 'root', '');
}
catch(Exception $e)
{ // En cas d'erreur, on affiche un message et on arrête tout
  die('Erreur : '.$e->getMessage());
}

$stmt = $bdd->prepare('SELECT * FROM User WHERE Id_user= ?');
$stmt->bindParam(1, $_SESSION['Id_user']);
$stmt->execute();

while($donne = $stmt->fetch(PDO::FETCH_ASSOC))
{
?>
<center>
<div class="table-responsive" style="width: 700px;">
<table class="table text-center table-bordered">
  <thead>
    <tr>
      <th scope="col" colspan="2" class="bg-info">Informations Personnelles</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Nom</th>
      <td><?php  echo $donne['Nom'];?></td>
    </tr>
    <tr>
      <th scope="row">Prenom</th>
      <td><?php  echo $donne['Prenom'];?></td>
    </tr>
    <tr>
      <th scope="row">Identifiant</th>
      <td><?php  echo $donne['Identifiant'];?></td>
    </tr>
    <tr>
      <th scope="row">Mail</th>
      <td><?php  echo $donne['Mail'];?></td>
    </tr>
    <tr>
      <th scope="row">Mot de passe</th>
      <td><?php  echo $donne['Mdp'];?></td>
    </tr>
  </tbody>
</table>
</div>
<a type="button" class="btn btn-outline-primary" href="modif_compte.php" aria-pressed="true">Modifier</a>
</center>
<?php
}
?>
