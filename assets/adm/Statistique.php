<?php include("menu.php"); ?>
</br>
<?php
  try
  { // On se connecte à MySQL
    $bdd = new PDO('mysql:host=localhost;dbname=Mini-pinterest;charset=utf8', 'root', '');
  }
  catch(Exception $e)
  { // En cas d'erreur, on affiche un message et on arrête tout
    die('Erreur : '.$e->getMessage());
  }
  // Si tout va bien, on peut continuer

  // On récupère tout le contenu de la table mini-pinterest
  $nbUser = $bdd->query('SELECT COUNT(*) as nb FROM User');
  $nbImage = $bdd->query('SELECT u.Nom, u.Prenom, u.Id_user, COUNT(p.Id_user) as nb FROM User u LEFT OUTER JOIN Photo p ON u.Id_user=p.Id_user GROUP BY u.Nom');
  $nbImageCat = $bdd->query('SELECT nomCat, COUNT(p.Id_user) as nbc FROM Categorie NATURAL JOIN Photo p GROUP BY nomCat');

  $nbUsers = $nbUser->fetch();
  $bdd = null;
?>

<center>
<div class="table-responsive" style="width: 700px;">
<table class="table text-center table-bordered">
  <thead>
    <tr>
      <th scope="col" colspan="2" class="bg-info">Nombre d'utilisateurs
        <span class="badge badge-primary badge-pill"><?php echo $nbUsers["nb"];?></span>
      </th>
    </tr>
  </thead>
  <?php
    while ($nbImageUser = $nbImage->fetch()){
  ?>
  <tbody>
    <tr>
      <th scope="row"><?php echo $nbImageUser["Nom"];?>
                      <?php echo $nbImageUser["Prenom"]?>
      </th>
    <td>  <A HREF="photo_utilisateur.php?id=<?php echo $nbImageUser["Id_user"]?>">
        <span class="badge badge-primary badge-pill"><?php echo $nbImageUser["nb"]; echo" "; echo "photo(s)";?></span>
      </A>
    </td>
     </tr>
     <?php
     }
     ?>
     <thead>
       <tr>
         <th scope="col" colspan="2" class="bg-info">Catégories
           <span class="badge badge-primary badge-pill"> 3 </span>
         </th>
       </tr>
       <?php
         while ($nbImageCategorie = $nbImageCat->fetch()){
       ?>
       <tr>
         <th scope="row"><?php echo $nbImageCategorie["nomCat"];?>  </th>
         <td>
             <span class="badge badge-primary badge-pill">  <?php echo $nbImageCategorie["nbc"]; echo " "; echo "photo(s)"?></span>
         </td>
       </tr>
       <?php
       }
       ?>
  </tbody>
</table>
</div>
</br>
</br>
<i>clique sur le nombre de photos d'un utilisateur pour voir les photos en questions ! :)</i>
</center>
