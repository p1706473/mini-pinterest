<?php
session_start();
//$image = imagecreate(200,50);
$targetDir = '../images/';
?>
<!DOCTYPE html>
<html lan="fr">

<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="style.css">
 <title>Mille et une Images</title>
</head>


<?php

if (isset($_FILES['pNom']) AND $_FILES['pNom']['error'] == 0)
{
  // Testons si le fichier n'est pas trop gros
  if ($_FILES['pNom']['size'] <= 100000)
  {
    // Testons si l'extension est autorisée
    $infosfichier = pathinfo($_FILES['pNom']['name']);
    $extension_upload = $infosfichier['extension'];
    $extensions_autorisees = array('jpeg', 'gif', 'png');
    if (in_array($extension_upload, $extensions_autorisees))
    {
      // On peut valider le fichier et le stocker définitivement
     if(move_uploaded_file($_FILES['pNom']['tmp_name'], $targetDir.basename($_FILES['pNom']['name'])))
     {
       $_SESSION['nomImage']=$_FILES['pNom']['name'];
       header('Location: formulaire_ajout.php');
     }
    }
    else
    {
      echo 'OUPS ! Cette extension de fichier n\'est pas autoris&ée. Votre fichier doit être en jpg, jpeg, gif ou png !';
      ?> </br><a href="ajouterimage.php" class="btn btn-primary btn-lg active" role="button" aria-pressed="true"> Essayer à nouveau </a>
      <?php
    }
  }
  else{
    echo 'OUPS! Votre fichier est trop lourd. Il ne doit pas dépasser 100ko.';
    ?></br> <a href="ajouterimage.php" class="btn btn-primary btn-lg active" role="button" aria-pressed="true"> Essayer à nouveau </a>
    <?php
  }
}
else {
  echo 'OUPS! Une erreur est survenue. Merci de réessayer';
  ?> </br><a href="ajouterimage.php" class="btn btn-primary btn-lg active" role="button" aria-pressed="true"> Essayer à nouveau </a>
  <?php
}

?>
