<?php include("menu.php"); ?>
<?php
try
{ // On se connecte à MySQL
  $bdd = new PDO('mysql:host=localhost;dbname=Mini-pinterest;charset=utf8', 'root', '');
}
catch(Exception $e)
{ // En cas d'erreur, on affiche un message et on arrête tout
  die('Erreur : '.$e->getMessage());
}
// Si tout va bien, on peut continuer

$stmt = $bdd->prepare('SELECT * FROM Photo p WHERE nomFich= ?');
$stmt->bindParam(1, $_GET['n']);
$stmt->execute();

$donne = $stmt->fetch(PDO::FETCH_ASSOC);

$stmt2 = $bdd->prepare('SELECT c.nomCat FROM Categorie c JOIN Photo p ON c.catId=p.catId WHERE p.nomFich = ?');
$stmt2->bindParam(1, $_GET['n']);
$stmt2->execute();

$donne2 = $stmt2->fetch(PDO::FETCH_ASSOC);

?>
<div class="container">
  <div class="justify-content-center">
    <div class="align-center">
      <div class="card" style="width: 22rem;">
        <img class="card-img-top" src="../images/<?php echo $donne['nomFich'];?>" alt="Card image cap">
        <div class="card-body">
          <center><h5 class="card-title"><?php echo $donne['nomFich']; ?>
            <?php
              if($donne["Visible"] == 1){
            ?>
                <a href="cacher_image.php?n=<?php echo $donne['nomFich'];?>">
                  <img
                    align="right";
                    style="cursor:hand;"
                    height = 25 px;
                    src = ../images/eye.png>
                  </img>
            <?php
            }
            else{
            ?>
            <a href="cacher_image.php?n=<?php echo $donne['nomFich'];?>">
              <img
                align="right";
                style="cursor:hand;"
                height = 25 px;
                src = ../images/eyeS.png>
              </img>
              <?php
              }
              ?>
          </a></h5></center>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item"><?php echo $donne['description']; ?></li>
            <li class="list-group-item">Catégorie: <?php echo $donne2['nomCat']; ?></li>
        </ul>
        <div class="card-body">
          <center>
            <a href="modif_image.php?n=<?php echo $donne['nomFich'] ?>" class="btn btn-outline-info">Modifier</a></br></br>
            <a href="suppr_image.php?n=<?php echo $donne['nomFich'] ?>" class="btn btn-outline-danger">Supprimer</a>
          </center>
        </div>
      </div>
    </div>
  </div>
</div>
