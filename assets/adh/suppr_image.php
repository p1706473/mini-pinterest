<?php include("menu.php"); ?>
<?php
try
{ // On se connecte à MySQL
  $bdd = new PDO('mysql:host=localhost;dbname=Mini-pinterest;charset=utf8', 'root', '');
}
catch(Exception $e)
{ // En cas d'erreur, on affiche un message et on arrête tout
  die('Erreur : '.$e->getMessage());
}

$stmt = $bdd->prepare('DELETE FROM Photo WHERE nomFich=?');
$stmt->bindParam(1, $_GET['n']);
$stmt->execute();

$donne = $stmt->fetch(PDO::FETCH_ASSOC);

header('Location: photo_user.php');
?>
