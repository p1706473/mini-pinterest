<?php session_start(); ?>

<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Mille et une Images</title>
  <h1>Mille et une Images</h1>
  <link rel="stylesheet" href="../../style.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <script src="script.js"></script>

  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Mini-Pinterest</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="accueil.php">Accueil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link">  <?php echo $_SESSION['nom']; echo " "; echo $_SESSION['prenom']; ?> </a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="../../index.php">Déconnexion</a>
      </li>
      </ul>
    </div>
  </nav>
</head>

<body>
  <center>
    <h4>Ajouter Une Image</h4></br>
    <form action="test_ajout_image.php" method="post" enctype="multipart/form-data">

        <div class="col-auto">
          <label for="nomFich">Choisissez un fichier</label>
          <input type="file" name="pNom" class="form-control-file" id="nomFich">
        </div>

      <button type="submit" class="btn btn-success" name="pEnvoyer" value="Valider">Envoyer</button>

    </form>

</center>


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>
