-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 30, 2020 at 08:15 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Mini-pinterest`
--

-- --------------------------------------------------------

--
-- Table structure for table `Adherent`
--

CREATE TABLE `Adherent` (
  `Id_Adherent` int(11) NOT NULL,
  `Id_user` int(11) DEFAULT NULL,
  `NbAdherents` int(11) DEFAULT NULL,
  `Nb_Photo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Adherent`
--

INSERT INTO `Adherent` (`Id_Adherent`, `Id_user`, `NbAdherents`, `Nb_Photo`) VALUES
(1, 1, 1, 0),
(2, 2, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Administrateur`
--

CREATE TABLE `Administrateur` (
  `Id_Admin` int(11) NOT NULL,
  `Id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Administrateur`
--

INSERT INTO `Administrateur` (`Id_Admin`, `Id_user`) VALUES
(1, 4),
(2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `Categorie`
--

CREATE TABLE `Categorie` (
  `catId` int(11) NOT NULL,
  `nomCat` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Categorie`
--

INSERT INTO `Categorie` (`catId`, `nomCat`) VALUES
(1, 'Animaux'),
(2, 'Autre'),
(3, 'Héroïnes');

-- --------------------------------------------------------

--
-- Table structure for table `Photo`
--

CREATE TABLE `Photo` (
  `photoId` int(11) NOT NULL,
  `nomFich` varchar(250) NOT NULL,
  `description` varchar(250) NOT NULL,
  `catId` int(11) NOT NULL,
  `Id_user` int(11) NOT NULL,
  `Visible` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Photo`
--

INSERT INTO `Photo` (`photoId`, `nomFich`, `description`, `catId`, `Id_user`, `Visible`) VALUES
(1, 'pandaroux.jpg', 'Le Petit panda, Panda roux ou Panda éclatant est un mammifère de la famille des Ailuridés.', 1, 1, 1),
(2, 'quokka.jpg', 'Le quokka est un petit marsupial de la famille des macropodidés. Il provient d\'Australie et est célèbre car il a l\'air de sourire perpétuellement.', 1, 2, 1),
(3, 'tchoupisson.jpg', 'Bébé hérisson', 1, 1, 1),
(4, 'babyyoda.jpg', 'Personnage de la série The Mandalorian, qui a fait l\'unanimité auprès des fans de part sa mignonnitude', 2, 2, 1),
(5, 'daria.jpg', 'Trent, le frère aîné de Jane Lane, la meilleure amie du personnage Daria, dans la série \"Daria\".', 2, 1, 1),
(6, 'disneymeme.jpg', 'meme tiré du dessin animé \"Aladdin\"', 2, 2, 1),
(7, 'eowyn.jpg', 'Personnage important de la saga \"Le Seigneur des Anneaux\".', 3, 1, 1),
(8, 'hermionegranger.jpg', 'Meilleure amie d\'Harry Potter, personnage principal de la saga du même nom. ', 3, 2, 1),
(9, 'spidergwen.jpg', 'Héroïne du comics Spider-Gwen, monde alternatif  où Peter Parker est mort et où Gwen Stacy est devenue une super-héroïne. ', 3, 1, 1),
(13, 'Chocola-et-Vanilla.jpeg', 'Manga qui raconte les aventures de Chocola et Vanilla, deux petites sorcières prétendantes au trône du royaume magique et prêtes à déployer tous leurs charmes pour s\'en emparer ', 3, 2, 0),
(15, 'Ramona.jpeg', 'Seule personnage intéressant du manga et film Scott Pilgrim VS The World', 3, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `Id_user` int(11) NOT NULL,
  `Nom` varchar(100) DEFAULT NULL,
  `Prenom` varchar(100) DEFAULT NULL,
  `Identifiant` varchar(100) DEFAULT NULL,
  `Mail` varchar(100) DEFAULT NULL,
  `Mdp` varchar(100) DEFAULT NULL,
  `Profil` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`Id_user`, `Nom`, `Prenom`, `Identifiant`, `Mail`, `Mdp`, `Profil`) VALUES
(1, 'Oplia', 'Blandine', 'blandineO', 'blandineoplia@mail.com', 'mdp17', 'Adherent'),
(2, 'Horil', 'Manu', 'manuH', 'manuhoril@mail.com', 'mdp18', 'Adherent'),
(3, 'Plios', 'Caroline', 'caroP', 'carolineplios@mail.com', 'mdp19', 'Adherent'),
(4, 'Couturier-Petrasson', 'Claire', 'AdminC', 'clairecouturierpetrasson@mail.com', 'mdpadmin2', 'Administrateur');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Photo`
--
ALTER TABLE `Photo`
  ADD PRIMARY KEY (`photoId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Photo`
--
ALTER TABLE `Photo`
  MODIFY `photoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
