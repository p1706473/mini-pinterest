COUTURIER-PETRASSON Claire p1710714
UCAR Ibrahim p1706473

Nous avons eu énormément de problèmes avec XAMPP/WAMPP/LAMPP, notamment des fonctionnalités qui fonctionnent chez l'un des membres du binôme mais pas chez l'autre, des problèmes de droits (avec lampp) et de compatibilité, d'où le fait que le nombre de commit ne correspond pas nécéssairement au nombre de fonctionnalités terminées.
Nous n'avons découvert l'existence de la maquette de départ que bien après le début du projet (nous en étions à plus de la moitié), aussi nous ne l'avons pas du tout utilisée.
Toutes les fonctionnalités demandées dans le sujet sont présentes et fonctionnent.
"adhérent" correspond à "utilisateur" dans le sujet: nous l'avons renommé car nous avons une table "user" qui fait le lien entre administrateur et utilisateur et nous voulions être sûrs que les tables soient bien distinctes.

- Pour cacher une photo, cliquer sur l'oeil en dessous de la photo. S'il est barré, la photo n'est visible que par celui ou celle qui l'a postée.
- L'administrateur peut voir/modifier/supprimer toutes les images, même celles qui sont cachées.
- Dans les statistiques, si on clique sur le nombre de photos d'un utilisateur, on est immédiatement redirigé sur les photos qu'il a posté.
- Attention lors de la suppression : il n'y a pas de boite de dialogue demandant si l'utilisateur / administrateur est sûr de vouloir supprimer l'image.

-----------------------------------------------------------------
Pour vous connecter :
- accès utilisateur:
  identifiant: manuH; mot de passe: mdp18

- accès administrateur:
  identifiant: AdminC; mot de passe: mdpadmin2
