<?php session_start();?>
<?php
try
{ // On se connecte à MySQL
  $bdd = new PDO('mysql:host=localhost;dbname=Mini-pinterest;charset=utf8', 'root', '');
}
catch(Exception $e)
{ // En cas d'erreur, on affiche un message et on arrête tout
  die('Erreur : '.$e->getMessage());
}
//include('connexionBD.php');
?>
    <!DOCTYPE html>
<html lan="fr">

    <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css">
     <title>Mille et une Images</title>
</head>

<?php

if(isset($_POST['pEnvoyer']) == FALSE){
        header('Location: http://localhost/mini-pinterest/erreur.php');
        echo "une erreur est survenue. Merci de vous connecter depuis : html/index.html";
    }
    else
        {
            $stmt = $bdd->prepare('SELECT * FROM User WHERE Identifiant = ? AND Mdp = ?');
            $stmt->bindParam(1, $_POST['pLogin']);
            $stmt->bindParam(2, $_POST['pPwd']);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_OBJ);
            //$stmt->bindResult( $r_identifiant, $r_motdepasse, $r_Profil);

            if(!$result) {
                header('Location: http://localhost/mini-pinterest/erreur.php');
                echo "une erreur est survenue.";
            }
            else{
                $_SESSION["Identifiant"] =  $_POST["pLogin"];
                $_SESSION["Mdp"] =  $_POST["pPwd"];
                if($result->Profil == "Administrateur")
                    {
                        $_SESSION['nom'] = "$result->Nom";
                        $_SESSION['prenom'] = "$result->Prenom";
                        $_SESSION['Id_user'] = "$result->Id_user";
                        $bdd = null;
                        $_SESSION['time'] = strtotime(date('h:i:s'));
                        header("Location: assets/adm/accueil.php");

                    }
                else if($result->Profil == "Adherent")
                    {
                        $_SESSION['nom'] = $result->Nom;
                        $_SESSION['prenom'] = "$result->Prenom";
                        $_SESSION['Id_user'] = "$result->Id_user";
                        $bdd = null;
                        $_SESSION['time'] = strtotime(date('h:i:s'));
                        header('Location: assets/adh/accueil.php');
                    }
            }
        }
?>
